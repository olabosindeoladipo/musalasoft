db.getSiblingDB('admin').auth(
    process.env.MONGO_INITDB_ROOT_USERNAME,
    process.env.MONGO_INITDB_ROOT_PASSWORD
);
default_db = db.getSiblingDB("DEFAULT_DB");
default_db.createCollection("drones");
default_db.createCollection("drones_log");
default_db.drones.createIndex({ "serialNumber": 1, "medications.name": 1, "medications.code": 1 }, { unique: true, partialFilterExpression: { "medications.name": { $exists: true }, "medications.code": { $exists: true } } });
default_db.drones.createIndex({ "serialNumber": 1 }, { unique: true });
default_db.drones_log.createIndex({ "droneId": 1, "timestamp": 1 })

