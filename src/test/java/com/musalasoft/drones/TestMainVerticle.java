package com.musalasoft.drones;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.musalasoft.drones.controllers.DroneController;
import com.musalasoft.drones.exceptions.BadRequestException;
import com.musalasoft.drones.models.Drone;
import com.musalasoft.drones.models.Medication;
import com.musalasoft.drones.services.impl.DroneServiceImpl;
import com.musalasoft.drones.utils.Utils;

import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;

@ExtendWith(VertxExtension.class)
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)

public class TestMainVerticle {
  private WebClient webClient;

  @Mock
  private DroneServiceImpl droneService;

  @Mock
  private RoutingContext routingContext;

  @BeforeEach
  void deploy_verticle(Vertx vertx, VertxTestContext testContext) {

    vertx.deployVerticle(new MainVerticle(), testContext.succeeding(id -> {
      MockitoAnnotations.openMocks(this);

      DroneController.init(droneService);
      webClient = WebClient.create(vertx);
      testContext.completeNow();
    }));
  }

  @Test
  void verticle_deployed(Vertx vertx, VertxTestContext testContext) throws Throwable {
    testContext.completeNow();
  }

  @Test
  void testGetDroneTotalWeightOfMedication(VertxTestContext testContext) {
    Drone drone = new Drone();

    Medication m1 = new Medication();
    m1.setWeight(40.0);
    Medication m2 = new Medication();
    m2.setWeight(30.0);
    List<Medication> medications = new ArrayList<Medication>();

    medications.add(m1);
    medications.add(m2);

    drone.setMedications(medications);
    assertEquals(Utils.getDroneTotalWeightOfMedication(drone), 70.0);
    testContext.completeNow();
  }

  @Test
  void testGetPathParamValidation() {
    when(routingContext.pathParams()).thenReturn(new HashMap<String,String>());

   Exception e= assertThrows(BadRequestException.class,()->Utils.validateIdFromPathParams(routingContext));
   assertTrue(e instanceof BadRequestException);
  }

  @Test
  void testGetQueryParamValidation() throws BadRequestException, JsonMappingException, JsonProcessingException {
    when(routingContext.queryParams()).thenReturn(MultiMap.caseInsensitiveMultiMap());
   assertInstanceOf(JsonObject.class, Utils.validateJsonToDroneJson(routingContext));
  }

  // @Test
  // void testCreateDrone(Vertx vertx, VertxTestContext testContext) {

  // JsonObject request = new JsonObject()
  // .put("serialNumber", "7484-94884-ASFU")
  // .put("model", "Lightweight")
  // .put("weightLimit", 90.0)
  // .put("batteryCapacity", 50.0);

  // JsonObject result = new JsonObject()
  // .put("serialNumber", "7484-94884-ASFU")
  // .put("model", "Lightweight")
  // .put("weightLimit", 90.0)
  // .put("batteryCapacity", 50.0)
  // .put("state", "IDLE")
  // .put("medications", new JsonArray());

  // Promise<JsonObject> createdDrone = Promise.promise();

  // Drone drone = new Drone();
  // drone.setSerialNumber("7484-94884-ASFU");
  // drone.setModel(DroneModel.valueOf("Lightweight"));
  // drone.setWeightLimit(90.0);
  // drone.setBatteryCapacity(50.0);
  // createdDrone.complete(result);

  // when(droneService.create(drone)).thenReturn(createdDrone);
  // webClient.post(8888, "localhost",
  // "/drones").timeout(60000).sendJsonObject(request).onSuccess(response -> {
  // assertEquals(response.statusCode(), 200);
  // testContext.completeNow();
  // }).onFailure(e -> {
  // e.printStackTrace();
  // testContext.failNow(e);
  // });
  // Mockito.verify(DroneServiceImpl.class, Mockito.calls(1));

  // //
  // when(droneService.create(drone).future()).thenReturn(createdDrone.future());
  // // doReturn(createdDrone).when(droneService.create(drone)).complete(result);
  // testContext.completeNow();
  // }

}