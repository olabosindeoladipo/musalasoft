package com.musalasoft.drones.models;

public enum DroneModel {
    Lightweight,
    Middleweight,
    Cruiserweight,
    Heavyweight
}
