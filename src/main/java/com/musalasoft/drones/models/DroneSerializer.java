package com.musalasoft.drones.models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.musalasoft.drones.utils.Mapper;

public class DroneSerializer extends StdDeserializer<Drone> {

    public DroneSerializer() {
        this(null);
    }

    public DroneSerializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Drone deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {

        JsonNode droneNode = jp.getCodec().readTree(jp);
        Drone drone = new Drone();
        drone.setId(droneNode.has("_id") ? droneNode.get("_id").textValue() : null);
        drone.setSerialNumber(
                droneNode.has("serialNumber") ? droneNode.get("serialNumber").textValue() : null);
        drone.setBatteryCapacity(
                droneNode.has("batteryCapacity") ? droneNode.get("batteryCapacity").doubleValue() : null);
        drone.setWeightLimit(droneNode.has("weightLimit") ? droneNode.get("weightLimit").doubleValue() : null);
        drone.setModel(droneNode.has("model") ? DroneModel.valueOf(droneNode.get("model").textValue()) : null);
        drone.setState(droneNode.has("state") ? DroneState.valueOf(droneNode.get("state").textValue()) : null);
        JsonNode arrNode = droneNode.get("medications");
        List<Medication> medications = new ArrayList<Medication>();
        if (arrNode != null && arrNode.isArray()) {
            for (JsonNode node : arrNode) {
                if (!node.isNull()) {
                    Medication m = Mapper.getMapper().readValue(node.toString(), Medication.class);
                    medications.add(m);
                }
            }

        }
        drone.setMedications(droneNode.has("medications") ? medications : null);
        return drone;
    }
}
