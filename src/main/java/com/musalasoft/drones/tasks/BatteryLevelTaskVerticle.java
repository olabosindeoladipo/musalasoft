package com.musalasoft.drones.tasks;

import com.musalasoft.drones.utils.Utils;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;

public class BatteryLevelTaskVerticle extends AbstractVerticle {
    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        JsonObject dbConfig = new JsonObject()
                .put("username", System.getenv("DB_USERNAME"))
                .put("password", System.getenv("DB_PASSWORD"))
                .put("authSource", "admin");
        DatabaseService.init(vertx, dbConfig);
        new RedisService().init(vertx);
        // 12 hrs
        vertx.setPeriodic(43200000, Utils::runBatteryLoggingTask);
        System.out.println("Task verticle deployed");
    }

}
