package com.musalasoft.drones.tasks;

import com.musalasoft.drones.utils.Utils;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class Controller {
    public static void getLogs(RoutingContext ctx) {
        try {
            new DatabaseService().getDronesLog(new JsonObject()).future().onSuccess(l -> {
                JsonArray logs = new JsonArray(l);
                ctx.response()
                        .putHeader("content-type", "application/json")
                        .end(logs.encode());
            });
        } catch (Exception e) {
            Utils.handleException(e, ctx);
        }
    }
}
