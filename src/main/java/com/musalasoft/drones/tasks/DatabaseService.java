package com.musalasoft.drones.tasks;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.BulkOperation;
import io.vertx.ext.mongo.MongoClient;

public class DatabaseService {
    private static MongoClient client;

    public static void init(Vertx vertx, JsonObject config) {
        if (client == null) {
            client = MongoClient.create(vertx, config);
        }
    }

    public MongoClient getClient() {
        return client;
    }

    public Promise<List<JsonObject>> getDrones(JsonObject filter) {
        Promise<List<JsonObject>> resultPromise = Promise.promise();
        client.find("drones", filter, res -> {
            if (res.failed()) {
                resultPromise.fail(res.cause());
            }
            resultPromise.complete(res.result());
        });
        return resultPromise;
    }

    public Promise<List<JsonObject>> getDronesLog(JsonObject filter) {
        Promise<List<JsonObject>> resultPromise = Promise.promise();
        client.find("drones_log", filter, res -> {
            if (res.failed()) {
                resultPromise.fail(res.cause());
            }
            resultPromise.complete(res.result());
        });
        return resultPromise;
    }

    public Promise<Boolean> updateTask() {
        Promise<Boolean> resultPromise = Promise.promise();
        getDrones(new JsonObject()).future()
                .onSuccess(res -> {
                    List<BulkOperation> operations = res.stream().map(c -> {
                        BatteryLog log = new BatteryLog();
                        log.setDroneId(c.getString("_id"));
                        log.setLevel(c.getDouble("batteryCapacity"));
                        log.setTimestamp(Timestamp.from(Instant.now()));

                        return BulkOperation.createInsert(JsonObject.mapFrom(log));
                    }).collect(Collectors.toList());

                    if (operations.size() > 0) {
                        client.bulkWrite("drones_log", operations).onSuccess(c -> {
                            resultPromise.complete(true);
                        }).onFailure(e -> {
                            resultPromise.fail(e);
                        });
                    } else {
                        resultPromise.complete();
                    }
                })
                .onFailure(f -> {
                    resultPromise.fail(f);
                });
        return resultPromise;
    }
}
