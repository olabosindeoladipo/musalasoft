package com.musalasoft.drones.tasks;

import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.redis.client.Redis;
import io.vertx.redis.client.RedisAPI;
import io.vertx.redis.client.Response;

public class RedisService {
    public static RedisAPI redis;

    public void init(Vertx vertx) {
        redis = RedisAPI.api(Redis.createClient(vertx));
    }

    public static Future<Response> saveToCache(Pair<String, String> data) {
        return RedisService.redis.set(Arrays.asList(data.getKey(), data.getValue()));
    }

    public static Future<Response> getFromCache(String key) {
        return RedisService.redis.get(key);
    }

    public static Future<Response> isKeyPresent(String key) {
        return RedisService.redis.exists(Arrays.asList(key));
    }

    public static Future<Response> deleteFromCache(String key) {
        return RedisService.redis.del(Arrays.asList(key));
    }
}
