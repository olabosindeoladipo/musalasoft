package com.musalasoft.drones.exceptions;

public class WeightExceededException extends Exception {

    static String defaultMessage = "Weight limit has been exceeded";

    public WeightExceededException() {
        super(defaultMessage);
    }

    public WeightExceededException(String message) {
        super(message == null ? defaultMessage : message);
    }

}
