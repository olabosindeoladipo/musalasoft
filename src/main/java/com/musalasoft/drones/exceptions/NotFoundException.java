
package com.musalasoft.drones.exceptions;

public class NotFoundException extends Exception {
    static final String defaultMessage = "Not Found";

    public NotFoundException() {
        super(defaultMessage);
    }

    public NotFoundException(String message) {
        super(message == null ? defaultMessage : message);
    }
}
