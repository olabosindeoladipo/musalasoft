package com.musalasoft.drones.exceptions;

public class ServerException extends Exception {

    static String defaultMessage = "Server Error";

    public ServerException() {
        super(defaultMessage);
    }

    public ServerException(String message) {
        super(message == null ? defaultMessage : message);
    }

}
