package com.musalasoft.drones.exceptions;

public class BadRequestException extends Exception {
    static String defaultMessage = "Bad Request";

    public BadRequestException() {
        super(defaultMessage);
    }

    public BadRequestException(String message) {
        super(message == null ? defaultMessage : message);
    }
}
