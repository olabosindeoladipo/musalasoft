package com.musalasoft.drones.controllers;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.musalasoft.drones.exceptions.BadRequestException;
import com.musalasoft.drones.exceptions.NotFoundException;
import com.musalasoft.drones.exceptions.ServerException;
import com.musalasoft.drones.exceptions.WeightExceededException;
import com.musalasoft.drones.models.Drone;
import com.musalasoft.drones.models.DroneState;
import com.musalasoft.drones.models.Medication;
import com.musalasoft.drones.services.interfaces.DroneService;
import com.musalasoft.drones.utils.Mapper;
import com.musalasoft.drones.utils.Utils;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class DroneController {
    static DroneService droneService;

    public static void init(DroneService droneService) {
        DroneController.droneService = droneService;

    }

    public static void get(RoutingContext ctx) {
        try {

            String id = Utils.validateIdFromPathParams(ctx).getString("id");
            Utils.validateJsonToDroneJson(ctx);

            droneService.get(id, new JsonObject(), new JsonObject()).future().onSuccess(drone -> {
                if (drone == null) {
                    Utils.handleException(new NotFoundException("Drone not found"), ctx);
                    return;
                }
                ctx.response()
                        .putHeader("content-type", "application/json")
                        .end(drone.encode());

            }).onFailure(e -> {
                Utils.handleException(e, ctx);
            });
        } catch (Exception e) {
            Utils.handleException(e, ctx);
        }
    }

    public static void getBatteryLevel(RoutingContext ctx) {
        try {

            String id = Utils.validateIdFromPathParams(ctx).getString("id");

            droneService.get(id, new JsonObject().put("batteryCapacity", true), new JsonObject()).future()
                    .onSuccess(drone -> {
                        if (drone == null) {
                            Utils.handleException(new NotFoundException("Drone not found"), ctx);
                            return;
                        }
                        ctx.response()
                                .putHeader("content-type", "application/json")
                                .end(drone.encode());

                    }).onFailure(e -> {
                        Utils.handleException(e, ctx);
                    });
        } catch (Exception e) {
            Utils.handleException(e, ctx);
        }
    }

    public static void getMedications(RoutingContext ctx) {
        try {

            String id = Utils.validateIdFromPathParams(ctx).getString("id");

            droneService.get(id, new JsonObject().put("medications", true), new JsonObject()).future()
                    .onSuccess(drone -> {
                        if (drone == null) {
                            Utils.handleException(new NotFoundException("Drone not found"), ctx);
                            return;
                        }
                        ctx.response()
                                .putHeader("content-type", "application/json")
                                .end(drone.encode());

                    }).onFailure(e -> {
                        Utils.handleException(e, ctx);
                    });
        } catch (Exception e) {
            Utils.handleException(e, ctx);
        }
    }

    public static void getAll(RoutingContext ctx) {
        try {

            JsonObject queryParams = Utils.validateJsonToDroneJson(ctx);

            droneService.getAll(new JsonObject(), queryParams).future().onSuccess(d -> {
                JsonArray drones = new JsonArray(d);
                ctx.response()
                        .putHeader("content-type", "application/json")
                        .end(drones.encode());
            }).onFailure(e -> {
                Utils.handleException(e, ctx);
            });
        } catch (Exception e) {
            Utils.handleException(e, ctx);
        }
    }

    public static void getAllAvailableDrones(RoutingContext ctx) {
        try {

            droneService.getAllAvailable().future().onSuccess(d -> {
                JsonArray drones = new JsonArray(d);
                ctx.response()
                        .putHeader("content-type", "application/json")
                        .end(drones.encode());
            }).onFailure(e -> {
                Utils.handleException(e, ctx);
            });
        } catch (Exception e) {
            Utils.handleException(e, ctx);
        }
    }

    public static void post(RoutingContext ctx) {

        try {

            Drone drone = Utils.validatePostRequest(ctx);

            drone.setState(DroneState.IDLE);
            drone.setBatteryCapacity(100.0);

            List<Medication> list = new ArrayList<Medication>();
            drone.setMedications(list);
            droneService.create(drone).future()
                    .onSuccess(d -> {
                        try {
                            ctx.response()
                                    .putHeader("content-type", "application/json")
                                    .end(Mapper.getMapper().writeValueAsString(drone));
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                            Utils.handleException(e, ctx);
                        }
                    })
                    .onFailure(e -> {
                        e.printStackTrace();
                        Utils.handleException(e, ctx);
                    });
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof IllegalArgumentException) {
                Utils.handleException(new BadRequestException("Invalid Request"), ctx);
                return;
            }

            Utils.handleException(e, ctx);
        }

    }

    public static void update(RoutingContext ctx) {
        try {

            Drone drone = Utils.validateUpdateRequest(ctx);
            String id = ctx.pathParam("id");
            JsonObject query = new JsonObject().put("_id", id);
            JsonObject updates = ctx.body().asJsonObject();

            droneService.get(id, new JsonObject(), new JsonObject()).future().onSuccess(t -> {
                if (t == null) {
                    Utils.handleException(new NotFoundException("Drone not found"), ctx);
                    return;
                }
                Drone dr = null;

                try {
                    dr = Mapper.getMapper().readValue(t.encode(), Drone.class);
                } catch (JsonProcessingException e) {
                    Utils.handleException(new BadRequestException(), ctx);
                }

                // Drone not found
                if (dr == null) {
                    Utils.handleException(new NotFoundException("Drone not found"), ctx);
                    return;
                }

                // Battery capacity is below 25%
                if (dr.getBatteryCapacity() < 25) {
                    Utils.handleException(new ServerException("Battery capacity is below 25%"), ctx);
                    return;
                }

                // Check if drone is already fully loaded
                if (dr.getState() == DroneState.LOADED) {
                    Utils.handleException(new ServerException("Drone is fully loaded and out of space"), ctx);
                    return;
                }

                // Calculate for available space for incoming medication
                Double totalLoadedWeight = Utils.getDroneTotalWeightOfMedication(dr);
                Double totalncomingWeight = Utils.getDroneTotalWeightOfMedication(drone);
                Double availableWeight = dr.getWeightLimit() - totalLoadedWeight;

                // Incoming weight exceeds the available space on drone
                if (totalncomingWeight > availableWeight) {
                    Utils.handleException(
                            new WeightExceededException(
                                    "Weight of medication exceeds the available weight limit of the drone"),
                            ctx);
                    return;
                }

                // Update the state of the drone
                Double spaceLeftAfterNewLoading = (dr.getWeightLimit()
                        - (totalLoadedWeight + totalncomingWeight));

                droneService.update(query, updates).future()
                        .onSuccess(d -> {
                            try {
                                if (d.containsKey("doc_matched") && d.getInteger("doc_matched") > 0) {
                                    JsonObject stateUpdate = new JsonObject();
                                    if (spaceLeftAfterNewLoading > 0) {
                                        stateUpdate.put("state", "LOADING");
                                    } else if (spaceLeftAfterNewLoading == 0) {
                                        stateUpdate.put("state", "LOADED");
                                    }

                                    droneService.update(query, stateUpdate).future()
                                            .onSuccess(res -> {
                                                ctx.response()
                                                        .putHeader("content-type", "application/json")
                                                        .end(new JsonObject().put("message", "Update successful")
                                                                .encode());
                                            }).onFailure(e -> {
                                                Utils.handleException(e, ctx);
                                            });
                                } else {
                                    throw new NotFoundException("Drone not found");
                                }

                            } catch (NotFoundException e) {
                                Utils.handleException(e, ctx);
                            }
                        })
                        .onFailure(e -> {
                            Utils.handleException(e, ctx);
                        });

            }).onFailure(e -> {
                Utils.handleException(e, ctx);
            });

        } catch (Exception e) {
            Utils.handleException(e, ctx);
        }
    }
}
