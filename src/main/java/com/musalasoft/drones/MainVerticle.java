package com.musalasoft.drones;

import com.musalasoft.drones.controllers.DroneController;
import com.musalasoft.drones.services.DatabaseService;
import com.musalasoft.drones.services.impl.DroneServiceImpl;
import com.musalasoft.drones.services.interfaces.DroneService;
import com.musalasoft.drones.utils.Mapper;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;

public class MainVerticle extends AbstractVerticle {
  ApiRoutes router = new ApiRoutes(vertx);

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    JsonObject dbConfig = new JsonObject()
        .put("host", "127.0.0.1")
        .put("port", 27017)
        .put("username", System.getenv("DB_USERNAME"))
        .put("password", System.getenv("DB_PASSWORD"))
        .put("authSource", "admin");

    DatabaseService databaseService = new DatabaseService();
    databaseService.initClient(vertx, dbConfig);
    DroneService droneService = new DroneServiceImpl(databaseService);
    DroneController.init(droneService);

    Mapper.init();

    vertx.createHttpServer().requestHandler(router.initRoutes()).listen(8888, http -> {
      if (http.succeeded()) {
        DeploymentOptions options = new DeploymentOptions().setWorker(true);
        vertx.deployVerticle("com.musalasoft.drones.tasks.BatteryLevelTaskVerticle", options);
        startPromise.complete();
        System.out.println("HTTP server started on port 8888");
      } else {
        startPromise.fail(http.cause());
      }
    });
  }
}
