package com.musalasoft.drones;

import com.musalasoft.drones.controllers.DroneController;
import com.musalasoft.drones.tasks.Controller;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class ApiRoutes {
    private Vertx vertx;
    public Router router;

    public ApiRoutes(Vertx vertx) {
        this.vertx = vertx;
        this.router = Router.router(this.vertx);

    }

    private Route appRoute(HttpMethod method, String path) {
        return method == HttpMethod.GET
                ? this.router.route(method, path)
                        .handler(BodyHandler.create())
                        .produces("application/json")
                : this.router.route(method, path)
                        .handler(BodyHandler.create())
                        .produces("application/json")
                        .consumes("application/json");
    }

    public Router initRoutes() {

        // Define Routes
        appRoute(HttpMethod.GET, "/drones/logs").handler(Controller::getLogs);
        appRoute(HttpMethod.GET, "/drones/available").handler(t -> DroneController.getAllAvailableDrones(t));
        appRoute(HttpMethod.GET, "/drones/:id").handler(DroneController::get);
        appRoute(HttpMethod.GET, "/drones").handler(DroneController::getAll);
        appRoute(HttpMethod.GET, "/drones/:id/battery").handler(DroneController::getBatteryLevel);
        appRoute(HttpMethod.GET, "/drones/:id/medications").handler(DroneController::getMedications);
        appRoute(HttpMethod.POST, "/drones").handler(DroneController::post);
        appRoute(HttpMethod.PUT, "/drones/:id").handler(DroneController::update);

        return this.router;
    }

}
