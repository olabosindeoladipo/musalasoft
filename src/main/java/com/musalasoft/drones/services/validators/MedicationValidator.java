package com.musalasoft.drones.services.validators;

import java.util.regex.Pattern;

import com.musalasoft.drones.models.Medication;

import am.ik.yavi.builder.ValidatorBuilder;
import am.ik.yavi.core.Validator;

public class MedicationValidator {
    public static final Validator<Medication> getValidator = ValidatorBuilder.<Medication>of()
            .constraint(Medication::getId, "id", c -> c.notNull().notBlank())
            .constraint(Medication::getName, "name",
                    c -> c.notBlank().predicate(
                            t -> Pattern.matches("^[\\w]*[-_]*[\\w]*$", t),
                            "name",
                            "Name accepts only alpahabets, numbers, hyphens and underscore"))
            .constraint(Medication::getWeight, "weight", c -> c.notNull().greaterThan(0.0))
            .constraint(Medication::getCode, "code",
                    c -> c.notBlank().predicate(t -> Pattern.matches("[A-Z_]*[\\d]*[_]*", t),
                            "code",
                            "Code accepts only uppercase letters, numbers and undercore"))
            .constraint(Medication::getImage, "image", c -> c.notBlank().url())

            .build();
    public static final Validator<Medication> postValidator = ValidatorBuilder.<Medication>of()
            .constraintOnCondition((medication, constraintGroup) -> medication.getId() != null,
                    b -> b.constraint(Medication::getId, "id", c -> c.isNull()))
            .constraint(Medication::getName, "name",
                    c -> c.notBlank().predicate(
                            t -> Pattern.matches("^[\\w]*[-_]*[\\w]*$", t),
                            "name",
                            "Name accepts only alpahabets, numbers, hyphens and underscore"))
            .constraint(Medication::getWeight, "weight", c -> c.notNull().greaterThan(0.0))
            .constraint(Medication::getCode, "code",
                    c -> c.notBlank().predicate(t -> Pattern.matches("[A-Z_]*[\\d]*[_]*", t),
                            "code",
                            "Code accepts only uppercase letters, numbers and undercore"))
            .constraint(Medication::getImage, "image", c -> c.notBlank().url())

            .build();
    public static final Validator<Medication> updateValidator = ValidatorBuilder.<Medication>of()

            .constraintOnCondition((medication, constraintGroup) -> medication.getName() != null, b -> b.constraint(
                    Medication::getName, "name",
                    c -> c.notBlank().predicate(t -> Pattern.matches("^[\\w]*[-_]*[\\w]*$", t), "name",
                            "Name accepts only alpahabets, numbers, hyphens and underscore")))
            .constraintOnCondition((medication, constraintGroup) -> medication.getWeight() != null,
                    b -> b.constraint(Medication::getWeight, "weight", c -> c.notNull().greaterThan(0.0)))
            .constraintOnCondition((medication, constraintGroup) -> medication.getWeight() != null,
                    b -> b.constraint(Medication::getCode, "code",
                            c -> c.notBlank().predicate(t -> Pattern.matches("[A-Z_]*[\\d]*[_]*", t),
                                    "code",
                                    "Code accepts only uppercase letters, numbers and undercore")))
            .constraintOnCondition((medication, constraintGroup) -> medication.getImage() != null,
                    b -> b.constraint(Medication::getImage, "image", c -> c.notBlank().url()))

            .build();
}
