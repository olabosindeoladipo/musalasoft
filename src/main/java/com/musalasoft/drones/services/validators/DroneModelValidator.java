package com.musalasoft.drones.services.validators;

import org.apache.commons.lang3.EnumUtils;

import com.musalasoft.drones.models.DroneModel;

import am.ik.yavi.builder.ValidatorBuilder;
import am.ik.yavi.core.Validator;

public class DroneModelValidator {
    public static final Validator<DroneModel> validator = ValidatorBuilder.<DroneModel>of()
            .constraintOnTarget("model", c -> c.predicate(d -> EnumUtils.isValidEnum(DroneModel.class,
                    d.toString()), "", ""))
            .build();
}
