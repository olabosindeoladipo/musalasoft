package com.musalasoft.drones.services.validators;

import org.apache.commons.lang3.EnumUtils;

import com.musalasoft.drones.models.DroneState;

import am.ik.yavi.builder.ValidatorBuilder;
import am.ik.yavi.core.Validator;

public class DroneStateValidator {
    public static final Validator<DroneState> validator = ValidatorBuilder.<DroneState>of()
            .constraintOnTarget("state", c -> c.predicate(d -> EnumUtils.isValidEnum(
                    DroneState.class,
                    d.toString()), "", ""))
            .build();
}
