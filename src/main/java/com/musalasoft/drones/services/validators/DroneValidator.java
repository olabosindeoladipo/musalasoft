package com.musalasoft.drones.services.validators;

import com.musalasoft.drones.models.Drone;
import com.musalasoft.drones.utils.Utils;

import am.ik.yavi.builder.ValidatorBuilder;
import am.ik.yavi.core.Validator;

public class DroneValidator {
        public static final Validator<Drone> postValidator = ValidatorBuilder.<Drone>of()

                        .constraintOnCondition((drone, constraintGroup) -> drone.getId() != null,
                                        b -> b.constraint(Drone::getId, "_id", c -> c.isNull()))
                        .constraint(Drone::getSerialNumber, "serialNumber",
                                        c -> c.notNull().predicate(
                                                        t -> t.trim().length() >= 1 && t.trim().length() <= 100,
                                                        "serialNumber",
                                                        ""))
                        .constraint(Drone::getBatteryCapacity, "batteryCapacity", c -> c.notNull().predicate(
                                        t -> t >= 0 && t <= Utils.MAX_BATTERY_CAPACITY, "batteryCapacity",
                                        ""))
                        .constraint(Drone::getWeightLimit, "weightLimit",
                                        c -> c.notNull().predicate(t -> t >= 0 && t <= Utils.MAX_WEIGHT_LIMIT,
                                                        "weightLimit",
                                                        "value exceeds the limit " + Utils.MAX_WEIGHT_LIMIT + "g"))
                        .nest(Drone::getModel, "model", DroneModelValidator.validator)
                        .constraintOnCondition((drone, constraintGroup) -> drone.getState() != null,
                                        b -> b.constraintOnTarget("state", c -> c.isNull()))
                        .constraintOnCondition((drone, constraintGroup) -> drone.getMedications() != null,
                                        b -> b.constraintOnTarget("medications", c -> c.isNull()))
                        .build();

        public static final Validator<Drone> updateValidator = ValidatorBuilder.<Drone>of()
                        .constraintOnCondition((drone, constraintGroup) -> drone.getId() != null,
                                        b -> b.constraint(Drone::getId, "id", c -> c.isNull()))
                        .constraintOnCondition((drone, constraintGroup) -> drone.getSerialNumber() != null,
                                        b -> b.constraint(Drone::getSerialNumber, "serialNumber", c -> c.isNull()))
                        .constraintOnCondition((drone, constraintGroup) -> drone.getBatteryCapacity() != null,
                                        b -> b.constraint(Drone::getBatteryCapacity, "batteryCapacity",
                                                        c -> c.notNull().predicate(
                                                                        t -> t >= 0 && t <= Utils.MAX_BATTERY_CAPACITY,
                                                                        "batteryCapacity",
                                                                        "")))
                        .constraintOnCondition((drone,
                                        constraintGroup) -> drone.getWeightLimit() != null,
                                        b -> b.constraint(
                                                        Drone::getWeightLimit, "weightLimit",
                                                        c -> c.notNull().predicate(
                                                                        t -> t >= 0 && t <= Utils.MAX_WEIGHT_LIMIT,
                                                                        "weightLimit",
                                                                        "value exceeds the limit "
                                                                                        + Utils.MAX_WEIGHT_LIMIT
                                                                                        + "g")))
                        .constraintOnCondition((drone,
                                        constraintGroup) -> drone.getModel() != null,
                                        b -> b.nest(Drone::getModel, "model",
                                                        DroneModelValidator.validator))
                        .constraintOnCondition((drone,
                                        constraintGroup) -> drone.getState() != null,
                                        b -> b.nest(Drone::getState, "state",
                                                        DroneStateValidator.validator))
                        .constraintOnCondition((drone,
                                        constraintGroup) -> drone.getMedications() != null,
                                        b -> b.forEach(
                                                        Drone::getMedications, "medications",
                                                        MedicationValidator.updateValidator))
                        .build();
}
