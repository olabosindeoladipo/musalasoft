package com.musalasoft.drones.services.interfaces;

import java.util.List;

import com.musalasoft.drones.models.Medication;

import io.vertx.core.Promise;

public interface MedicationService {
    public Promise<Medication> create(Medication drone);

    public Promise<Medication> get(String id);

    public Promise<List<Medication>> getAll();
}
