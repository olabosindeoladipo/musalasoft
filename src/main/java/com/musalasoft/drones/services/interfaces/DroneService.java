package com.musalasoft.drones.services.interfaces;

import java.util.List;

import com.musalasoft.drones.models.Drone;

import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;

public interface DroneService {
    public Promise<JsonObject> create(Drone drone);

    public Promise<JsonObject> get(String id, JsonObject fields, JsonObject queries);

    public Promise<List<JsonObject>> getAll(JsonObject fields, JsonObject queries);

    public Promise<List<JsonObject>> getAllAvailable();

    public Promise<JsonObject> update(JsonObject query, JsonObject updates);
}
