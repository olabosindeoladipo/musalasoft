package com.musalasoft.drones.services.impl;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.musalasoft.drones.models.Drone;
import com.musalasoft.drones.services.DatabaseService;
import com.musalasoft.drones.services.interfaces.DroneService;
import com.musalasoft.drones.utils.Mapper;

import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;

public class DroneServiceImpl implements DroneService {
    DatabaseService databaseService;

    public DroneServiceImpl(DatabaseService service) {
        this.databaseService = service;
    }

    @Override
    public Promise<JsonObject> create(Drone drone) {
        Promise<JsonObject> resultPromise = Promise.promise();

        try {
            JsonObject droneJson = new JsonObject(Mapper.getMapper().writeValueAsString(drone));
            this.databaseService.dbClient.insert("drones", droneJson,
                    res -> {
                        if (res.succeeded()) {
                            String id = res.result();
                            droneJson.put("_id", id);
                            resultPromise.complete(droneJson);
                        } else {
                            resultPromise.fail(res.cause());
                        }
                    });
        } catch (JsonProcessingException e) {
            resultPromise.fail(e);
        }
        return resultPromise;
    }

    @Override
    public Promise<JsonObject> get(String id, JsonObject fields, JsonObject queries) {
        Promise<JsonObject> resultPromise = Promise.promise();

        JsonObject query = new JsonObject().put("_id", id);
        if (queries != null && !queries.isEmpty()) {
            query = query.mergeIn(queries);
        }

        JsonObject options = new JsonObject();
        options.put("fields", fields);
        FindOptions findOptions = new FindOptions(options);

        this.databaseService.dbClient.findWithOptions("drones", query, findOptions, res -> {
            if (res.succeeded()) {
                resultPromise.complete(res.result().size() > 0 ? res.result().get(0) : null);
            } else {
                resultPromise.fail(res.cause());
            }
        });
        return resultPromise;
    }

    @Override
    public Promise<List<JsonObject>> getAll(JsonObject fields, JsonObject queries) {
        Promise<List<JsonObject>> resultPromise = Promise.promise();

        JsonObject options = new JsonObject();
        options.put("fields", fields);
        FindOptions findOptions = new FindOptions(options);

        this.databaseService.dbClient.findWithOptions("drones", queries, findOptions,
                res -> {
                    if (res.succeeded()) {

                        resultPromise.complete(res.result());
                    } else {
                        resultPromise.fail(res.cause());
                    }
                });
        return resultPromise;
    }

    @Override
    public Promise<List<JsonObject>> getAllAvailable() {
        Promise<List<JsonObject>> resultPromise = Promise.promise();
        JsonObject stateQueryBlock = new JsonObject().put("$in", new JsonArray().add("LOADING").add("IDLE"));
        JsonObject batterQueryBlock = new JsonObject().put("$gte", 25.0);
        JsonObject query = new JsonObject().put("state", stateQueryBlock).put("batteryCapacity", batterQueryBlock);

        this.databaseService.dbClient.find("drones", query,
                res -> {
                    if (res.succeeded()) {

                        resultPromise.complete(res.result());
                    } else {
                        resultPromise.fail(res.cause());
                    }
                });
        return resultPromise;
    }

    @Override
    public Promise<JsonObject> update(JsonObject query, JsonObject updates) {
        Promise<JsonObject> resultPromise = Promise.promise();
        JsonObject processed = new JsonObject();
        if (updates.containsKey("medications") && updates.getJsonArray("medications") != null) {
            JsonObject medicationObject = new JsonObject().put("medications", new JsonObject().put("$each",
                    updates.getJsonArray("medications")));
            JsonObject operator = new JsonObject().put("$addToSet", medicationObject);

            updates.remove("medications");
            processed.mergeIn(operator);
            processed.put("$set", updates);
        } else {
            processed.put("$set", updates);
        }

        this.databaseService.dbClient.updateCollection("drones", query, processed, res -> {
            if (res.succeeded()) {
                resultPromise.complete(
                        res.result().toJson());

            } else {
                resultPromise.fail(res.cause());
            }
        });
        return resultPromise;
    }

}
