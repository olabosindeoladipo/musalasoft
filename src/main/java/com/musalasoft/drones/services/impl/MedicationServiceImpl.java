package com.musalasoft.drones.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.musalasoft.drones.models.Medication;
import com.musalasoft.drones.services.DatabaseService;
import com.musalasoft.drones.services.interfaces.MedicationService;
import com.musalasoft.drones.utils.Mapper;

import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;

public class MedicationServiceImpl implements MedicationService {
    DatabaseService databaseService;

    public MedicationServiceImpl(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    @Override
    public Promise<Medication> create(Medication medication) {
        Promise<Medication> resultPromise = Promise.promise();

        try {
            databaseService.dbClient.insert("medications",
                    new JsonObject(Mapper.getMapper().writeValueAsString(medication)), res -> {
                        if (res.succeeded()) {
                            String id = res.result();
                            medication.setId(id);
                            resultPromise.complete(medication);
                        } else {
                            resultPromise.fail(res.cause());
                        }
                    });
        } catch (JsonProcessingException e) {
            resultPromise.fail(e);
        }
        return resultPromise;
    }

    @Override
    public Promise<Medication> get(String id) {
        Promise<Medication> resultPromise = Promise.promise();
        JsonObject query = new JsonObject().put("_id", id);
        databaseService.dbClient.findOne("medications", query, null, res -> {
            if (res.succeeded()) {
                try {
                    resultPromise.complete(Mapper.getMapper().readValue(res.result().encode(), Medication.class));
                } catch (JsonProcessingException e) {
                    resultPromise.fail(res.cause());
                }
            } else {
                resultPromise.fail(res.cause());
            }
        });
        return resultPromise;
    }

    @Override
    public Promise<List<Medication>> getAll() {
        Promise<List<Medication>> resultPromise = Promise.promise();
        databaseService.dbClient.find("medications", new JsonObject(), res -> {
            if (res.succeeded()) {
                List<Medication> medicationList = res.result().stream()
                        .map(arg0 -> {
                            try {
                                return Mapper.getMapper().readValue(arg0.encode(), Medication.class);
                            } catch (JsonProcessingException e) {
                                resultPromise.fail(e);
                                return null;
                            }
                        })
                        .collect(Collectors.toList());
                resultPromise.complete(medicationList);
            } else {
                resultPromise.fail(res.cause());
            }
        });
        return resultPromise;
    }

}
