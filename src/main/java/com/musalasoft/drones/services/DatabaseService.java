package com.musalasoft.drones.services;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;

public class DatabaseService {
    public MongoClient dbClient;

    public void initClient(Vertx vertx, JsonObject config) {
        if (dbClient == null) {
            dbClient = MongoClient.create(vertx, config);
        }
    }

    public MongoClient getClient() {
        return this.dbClient;
    }

}