package com.musalasoft.drones.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Mapper {
    static ObjectMapper mapper;

    public static void init() {
        mapper = new ObjectMapper();
    }

    public static ObjectMapper getMapper() {
        return mapper;
    }
}
