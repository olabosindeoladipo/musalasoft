package com.musalasoft.drones.utils;

import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.musalasoft.drones.exceptions.BadRequestException;
import com.musalasoft.drones.exceptions.NotFoundException;
import com.musalasoft.drones.exceptions.ServerException;
import com.musalasoft.drones.exceptions.WeightExceededException;
import com.musalasoft.drones.models.Drone;
import com.musalasoft.drones.models.Medication;
import com.musalasoft.drones.services.validators.DroneValidator;
import com.musalasoft.drones.tasks.DatabaseService;
import com.musalasoft.drones.tasks.RedisService;

import am.ik.yavi.core.ConstraintViolations;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class Utils {
    public static final Double MAX_WEIGHT_LIMIT = 500.0;
    public static final Double MAX_BATTERY_CAPACITY = 100.0;

    public static void handleException(Throwable e, RoutingContext ctx) {
        if (e instanceof BadRequestException) {
            ctx.response().setStatusCode(400)
                    .putHeader("content-type", "application/json")
                    .end(new JsonObject()
                            .put("message", e.getMessage()).encode());
        } else if (e instanceof NotFoundException) {
            ctx.response().setStatusCode(404)
                    .putHeader("content-type", "application/json")
                    .end(new JsonObject()
                            .put("message", e.getMessage()).encode());
        } else if (e instanceof WeightExceededException) {
            ctx.response().setStatusCode(400)
                    .putHeader("content-type", "application/json")
                    .end(new JsonObject()
                            .put("message", e.getMessage()).encode());
        } else if (e instanceof ServerException) {
            ctx.response().setStatusCode(500)
                    .putHeader("content-type", "application/json")
                    .end(new JsonObject()
                            .put("message", e.getMessage()).encode());
        } else {
            ctx.response().setStatusCode(500)
                    .putHeader("content-type", "application/json")
                    .end(new JsonObject()
                            .put("message", "Server Error").encode());
        }
    }

    public static Double getDroneTotalWeightOfMedication(Drone drone) {
        Double totalWeight = 0.0;
        for (Medication medication : drone.getMedications()) {
            totalWeight += medication.getWeight();
        }
        return totalWeight;
    }

    public static JsonObject convertMultiMaptoJsonObject(MultiMap dataMap) {
        JsonObject json = new JsonObject();
        for (Map.Entry<String, String> entry : dataMap.entries()) {
            json.put(entry.getKey(), entry.getValue());
        }
        return json;
    }

    public static JsonObject convertMaptoJsonObject(Map<String, String> dataMap) {
        JsonObject json = new JsonObject();
        for (Entry<String, String> entry : dataMap.entrySet()) {
            json.put(entry.getKey(), entry.getValue());
        }
        return json;
    }

    public static void validateDronePathParams(RoutingContext ctx) throws BadRequestException {
        if (!ctx.pathParams().containsKey("id") || ctx.pathParam("id") == null || ctx.pathParam("id").isBlank()) {
            throw new BadRequestException("id parameter is required");
        }

        JsonObject pathParams = ctx.pathParams().isEmpty() ? new JsonObject()
                : Utils.convertMaptoJsonObject(ctx.pathParams());

        // validate path params
        try {
            Mapper.getMapper().readValue(pathParams.encode(), Drone.class);
        } catch (Exception e) {
            Utils.handleException(new BadRequestException(), ctx);
        }
    }

    public static void validateDroneQueryParams(RoutingContext ctx) {
        JsonObject queries = ctx.queryParams().isEmpty() ? new JsonObject()
                : Utils.convertMultiMaptoJsonObject(ctx.queryParams());

        // validate queries
        try {
            Mapper.getMapper().readValue(queries.encode(), Drone.class);
        } catch (Exception e) {
            Utils.handleException(new BadRequestException(), ctx);
        }
    }

    public static JsonObject validateIdFromPathParams(RoutingContext ctx)
            throws BadRequestException {
        // validate id
        if (!ctx.pathParams().containsKey("id") || ctx.pathParam("id") == null || ctx.pathParam("id").isBlank()) {
            throw new BadRequestException("id parameter is required");
        }

        JsonObject pathParams = ctx.pathParams().isEmpty() ? new JsonObject()
                : Utils.convertMaptoJsonObject(ctx.pathParams());

        return pathParams;
    }

    public static JsonObject validateJsonToDroneJson(RoutingContext ctx)
            throws BadRequestException, JsonMappingException, JsonProcessingException {

        JsonObject queries = ctx.queryParams().isEmpty() ? new JsonObject()
                : Utils.convertMultiMaptoJsonObject(ctx.queryParams());

        Mapper.getMapper().readValue(queries.encode(), Drone.class);

        return queries;
    }

    public static Drone validateJsonToDrone(RoutingContext ctx)
            throws BadRequestException, JsonMappingException, JsonProcessingException {

        JsonObject queries = ctx.queryParams().isEmpty() ? new JsonObject()
                : Utils.convertMultiMaptoJsonObject(ctx.queryParams());

        return Mapper.getMapper().readValue(queries.encode(), Drone.class);

    }

    public static Drone validatePostRequest(RoutingContext ctx)
            throws JsonMappingException, JsonProcessingException, BadRequestException {
        Drone drone = Mapper.getMapper().readValue(ctx.body().asString(), Drone.class);
        ConstraintViolations violations = DroneValidator.postValidator.validate(drone);

        if (!violations.isValid()) {

            String violationsString = violations.stream()
                    .map(t -> java.text.MessageFormat.format(t.defaultMessageFormat(), t.args()))
                    .collect(Collectors.joining(" | ")).replace("\"",
                            "'");
            throw new BadRequestException(violationsString);
        }
        return drone;
    }

    public static Drone validateUpdateRequest(RoutingContext ctx)
            throws BadRequestException, JsonMappingException, JsonProcessingException {
        Utils.validateDronePathParams(ctx);
        Drone drone = Utils.validateJsonToDrone(ctx);

        try {
            drone = Mapper.getMapper().readValue(ctx.body().asString(), Drone.class);
        } catch (Exception e) {
            throw new BadRequestException("Invalid Request");
        }

        ConstraintViolations violations = DroneValidator.updateValidator.validate(drone);

        if (!violations.isValid()) {

            String violationsString = violations.stream()
                    .map(t -> java.text.MessageFormat.format(t.defaultMessageFormat(), t.args()))
                    .collect(Collectors.joining(" | ")).replace("\"",
                            "'");
            throw new BadRequestException(violationsString);
        }
        return drone;
    }

    public static void runBatteryLoggingTask(long period) {
        // Check for existing task in cache
        RedisService.isKeyPresent("LOGGING_BATTERY").onComplete(res -> {
            if (res.succeeded()) {
                if (res.result().toInteger() == 0) {
                    System.out.println("Starting Task...");
                    RedisService.saveToCache(Pair.of("LOGGING_BATTERY", "true"));
                    new DatabaseService().updateTask().future()
                            .onComplete(u -> {
                                RedisService.deleteFromCache("LOGGING_BATTERY");
                                System.out.println("Finished Task");
                            })
                            .onFailure(e -> {
                                RedisService.deleteFromCache("LOGGING_BATTERY");
                                e.printStackTrace();
                            });
                }
            } else {
                res.cause().printStackTrace();
            }
        });
    }

}
