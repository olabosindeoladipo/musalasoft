FROM maven:3.8.5-openjdk-17
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
RUN mvn clean && mvn package
CMD ["java", "-jar", "target/drones-1.0.0-SNAPSHOT-fat.jar"]
EXPOSE 8888