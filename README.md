## Drones

---

:scroll: **START**

### System Requirements

To deploy this project,the following are required to be installed:

1. Docker and Docker Compose
2. JDK 17
3. Maven 3.9.2

### Build

To build the project open the terminal on your workstation and run the following command

1. Go to the root folder of the project

```
cd < root dir of the project >
```

2. Build the docker images for the application and the database

```
 docker-compose build
```

---

### Run

Run docker-compose command, as shown below to start the database

```
 docker-compose up

```

Then package and run the jar file along with the environment variables for DB authentication

```
export DB_PASSWORD=musala#123 && export DB_USERNAME=musala && mvn package && java -jar target/drones-1.0.0-SNAPSHOT-fat.jar

```

---

### Test

Tests are run before succesful deployment of the jar file

---

:scroll: **END**

```

```
